#!/bin/bash
#a valid file type variable based on the structure in kubeconfig-data.yaml should exist 
#cluster_context_spec should be specifed as the name of the file type variable

cluster_spec=$1

cluster_context_file="${!cluster_spec}"
mv $cluster_context_file ${cluster_context_file}.yaml
ytt -f kube/kubeconfig.yaml -f ${cluster_context_file}.yaml > ci_kubeconfig
export KUBECONFIG=ci_kubeconfig

